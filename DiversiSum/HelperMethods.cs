﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiversiSum
{
    class HelperMethods
    {
        public static double[][] UniformDistributionMatrix (double[][] mat)
        {
            double[][] result = new double[mat.Length][];

            for (int i = 0; i < mat.Length; i++)
            {
                result[i] = new double[mat[i].Length];
                for (int j = 0; j < mat[i].Length; j++)
                {
                    result[i][j] = (double) 1 / mat[i].Length;
                }
            }

            return result;
        }

        public static double[][] DyadicProduct(double[][] col, double[][] row)
        {
            double[][] result = new double[col.Length][];
            
            for (int i = 0; i < col.Length; i++)
            {
                result[i] = new double[col.Length];
                for (int j = 0; j < col.Length; j++)
                {
                    result[i][j] = col[i][0] * row[0][j];
                }
            }

            return result;
        }

        
    }
}
