﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Accord.Math;
using DiversiSumV1;
using MathNet.Numerics;

namespace DiversiSum
{
    class Program
    {


        static void Main(string[] args)
        {
            //double[,] P =
            //{
            //    { 1,2,3,4,5,2,3,6,1,0,3,6,0,5,4,1,6,3,7,0,6,0,3,6,2 },
            //    { 6,0,3,6,2,1,2,3,4,5,1,6,3,7,0,3,6,0,5,4,2,3,6,1,0 },
            //    { 1,6,3,7,0,2,3,6,1,0,3,6,0,5,4,1,2,3,4,5,6,0,3,6,2 },
            //    { 3,6,0,5,4,1,6,3,7,0,2,3,6,1,0,6,0,3,6,2,1,2,3,4,5 },
            //    { 2,3,6,1,0,1,2,3,4,5,6,0,3,6,2,1,6,3,7,0,3,6,0,5,4 },

            //    { 1,6,3,7,0,2,3,6,1,0,3,6,0,5,4,1,2,3,4,5,6,0,3,6,2 },
            //    { 3,6,0,5,4,1,6,3,7,0,2,3,6,1,0,6,0,3,6,2,1,2,3,4,5 },
            //    { 6,0,3,6,2,1,2,3,4,5,1,6,3,7,0,3,6,0,5,4,2,3,6,1,0 },
            //    { 2,3,6,1,0,1,2,3,4,5,6,0,3,6,2,1,6,3,7,0,3,6,0,5,4 },
            //    { 1,2,3,4,5,2,3,6,1,0,3,6,0,5,4,1,6,3,7,0,6,0,3,6,2 },

            //    { 1,2,3,4,5,2,3,6,1,0,3,6,0,5,4,1,6,3,7,0,6,0,3,6,2 },
            //    { 6,0,3,6,2,1,2,3,4,5,1,6,3,7,0,3,6,0,5,4,2,3,6,1,0 },
            //    { 1,6,3,7,0,2,3,6,1,0,3,6,0,5,4,1,2,3,4,5,6,0,3,6,2 },
            //    { 3,6,0,5,4,1,6,3,7,0,2,3,6,1,0,6,0,3,6,2,1,2,3,4,5 },
            //    { 2,3,6,1,0,1,2,3,4,5,6,0,3,6,2,1,6,3,7,0,3,6,0,5,4 },

            //    { 1,6,3,7,0,2,3,6,1,0,3,6,0,5,4,1,2,3,4,5,6,0,3,6,2 },
            //    { 3,6,0,5,4,1,6,3,7,0,2,3,6,1,0,6,0,3,6,2,1,2,3,4,5 },
            //    { 6,0,3,6,2,1,2,3,4,5,1,6,3,7,0,3,6,0,5,4,2,3,6,1,0 },
            //    { 2,3,6,1,0,1,2,3,4,5,6,0,3,6,2,1,6,3,7,0,3,6,0,5,4 },
            //    { 1,2,3,4,5,2,3,6,1,0,3,6,0,5,4,1,6,3,7,0,6,0,3,6,2 },

            //    { 2,3,6,1,0,1,2,3,4,5,6,0,3,6,2,1,6,3,7,0,3,6,0,5,4 },
            //    { 3,6,0,5,4,1,6,3,7,0,2,3,6,1,0,6,0,3,6,2,1,2,3,4,5 },
            //    { 1,6,3,7,0,2,3,6,1,0,3,6,0,5,4,1,2,3,4,5,6,0,3,6,2 },
            //    { 1,2,3,4,5,2,3,6,1,0,3,6,0,5,4,1,6,3,7,0,6,0,3,6,2 },
            //    { 6,0,3,6,2,1,2,3,4,5,1,6,3,7,0,3,6,0,5,4,2,3,6,1,0 },
            //};

            double[][] O = new double[][]
            {
                new double[] { 1,2,3,4,5 },
                new double[] { 6,2,0,6,2 },
                new double[] { 1,6,3,7,2 },
                new double[] { 3,6,2,0,4 },
                new double[] { 2,3,6,1,7 }
            };
            O.ToTransition(true);

            SparseMatrix<Complex> SMC = new SparseMatrix<Complex>(O.ToMatrix().Transpose().Apply(i => Complex.Conjugate(i)));

            SparseMatrixEigenvalueDecomposition ED = new SparseMatrixEigenvalueDecomposition(SMC);
            
            double[][] v = ED.Eigenvectors.ToMagnitude();


            double[][] r = v.Divide(v.Sum());

            for (int i = 0; i < r.Length; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < r[i].Length; j++)
                    Console.WriteLine(r[i][j]);
            }

            Console.WriteLine();

            foreach (double col in O.SumEachColumn())
                Console.WriteLine(col + " ");

            Console.WriteLine();


            for (int i = 0; i < O.Length; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < O[i].Length; j++)
                {
                    Console.WriteLine(O[i][j]);
                }
            }

            Console.WriteLine();

            double[][] U = Matrix.Zeros(5,1).ToJagged();
            for (int i = 0; i < U.Length; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < U[i].Length; j++)
                    Console.WriteLine(U[i][j]);
            }

            Console.WriteLine();

            int n = O.Length;

            // Compute the inverse with one row removed
            double[][] u = Matrix.Zeros(n, 1).ToJagged();
            u[0][0] = -1;

            double[][] z = new double[1][];
            v[0] = new double[O[0].Length];
            v[0][0] -= 1;

            double[][] s = O.Inverse();
            MathNet.Numerics.LinearAlgebra.Single.Matrix.

            for (int i = 0; i < s.Length; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < s[i].Length; j++)
                    Console.WriteLine(s[i][j]);
                
            }

            Console.ReadLine();
        }
    }
}
