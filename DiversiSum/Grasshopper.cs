﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Accord;
using Accord.Statistics;
using Accord.Math;

namespace DiversiSum
{
    class Grasshopper
    {
        private int n;
        private double[][] W;
        private double[][] uniformR;
        private double[][] R;
        private int lambda;
        private int k;

        public int[] rankedIndices;
        public double[] averageVisits;


        /// <summary>
        /// Stationary distribution of a transition matrix
        /// </summary>
        /// <param name="P">Input Matrix</param>
        /// <returns>The probability matrix of the Stationary Distribution</returns>
        private double[][] StationaryDistribution(double[,] P)
        {

            SparseMatrix<Complex> SMC = new SparseMatrix<Complex>(P.Transpose().Apply(i => Complex.Conjugate(i)));

            SparseMatrixEigenvalueDecomposition ED = new SparseMatrixEigenvalueDecomposition(SMC);


            double[][] v = ED.Eigenvectors.ToMagnitude();
 
            return v.Divide(v.Sum());
        }

        private double[][] MatrixInversionLemma(double[][] A, double[][] AInv, int index)
        {
            double[][] R = new double[A.Length][];

            int n = A.Length;

            // Compute the inverse with one row removed
            double[][] u = Matrix.Zeros(n, 1).ToJagged();
            u[index][0] = - 1;

            double[][] v = new double[1][];
            v[index] = new double[A[index].Length];
            v[index][index] -= 1;

            double[][] s = HelperMethods.DyadicProduct(AInv.ColumnMult(u, index), AInv.RowMult(v, index));
            double[][] T = AInv.Subtract(s); 

            return R;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="w">Adjacency Matrix of the input</param>
        /// <param name="R">Prior Ranking via probability distribution(Uniform if null)</param>
        /// <param name="Lambda">Set of [0,1]. Balances between the W and r</param>
        /// <param name="K">Number of results to return</param>
        public Grasshopper (double[][] w, int Lambda, int K, double[][] r = null)
        {


            // W = Matrix with values and their weights
            // r = Prior ranking (PageRank) in form of probability vector
            // lambda = 0 is W, 1 is r. Randomly switches to keep balance
            // k = number of lines to return
            n = w.Length;
            W = w;
            if (r == null)
                R = HelperMethods.UniformDistributionMatrix(W);
            else
                R = r.Transpose();
            lambda = Lambda;
            k = K;



            // Initial Checks
            if (W.Min() < 0)
                throw new Exception("Negative entry in W");
            else if (Math.Abs(R.Sum() - 1) > 1 * Math.Pow(10, -5))
                throw new Exception("Prior rank vector does not sum to 1");
            else if (lambda < 0 || lambda > 1)
                throw new Exception("Lambda is not binary");
            else if (k < 0 || k > n)
                throw new Exception("k is outside of the range of the matrix");


            // Creating 'P', the transition matrix for the graph 'W'
            double[][] P = W.ToTransition(false);
            
            
        }


    }
}