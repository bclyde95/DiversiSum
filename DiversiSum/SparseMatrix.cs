﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Accord.Math;

namespace DiversiSum
{
    public sealed class SparseMatrix<T> : IEnumerable<T>, ICloneable,
        IList<T>, IList, IFormattable
    {
        Dictionary<Tuple<int,int>,T> Map;
        private Tuple<int,int>[] indices;
        private T[] values;
        private int nonZeroLength;
        private int rowsLength;
        private int colsLength;

        /// <summary>
        /// Specifies the indexing for the class with two ints
        /// </summary>
        /// <param name="index1">row index</param>
        /// <param name="index2">column index</param>
        /// <returns>The value associated with the index</returns>
        public T this[int index1, int index2]
        {
            get
            {
                if (indices.Contains(new Tuple<int, int>(index1, index2)))
                    return Map[new Tuple<int, int>(index1, index2)];
                else
                {
                    if (index1 < rowsLength && index2 < colsLength)
                    {
                        return default(T);
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }

            }
            set
            {
                if (indices.Contains(new Tuple<int, int>(index1, index2)))
                    Map[new Tuple<int, int>(index1, index2)] = value;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        
        public T this[Tuple<int,int> index]
        {
            get
            {
                if (indices.Contains(index))
                    return Map[index];
                else
                {
                    if (index.Item1 < rowsLength && index.Item2 < colsLength)
                    {
                        return default(T);
                    }
                    else
                    {
                        throw new IndexOutOfRangeException();
                    }
                }

            }
            set
            {
                if (indices.Contains(index))
                    Map[index] = value;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        ///   Gets or sets the vector of indices indicating the location
        ///   of the non-zero elements contained in this sparse matrix.
        ///   Must set values first, if setting individually.
        /// </summary>
        /// 
        public Tuple<int,int>[] Indices
        {
            get { return indices; }
            set { indices = value; UpdateMap(); }
        }

        /// <summary>
        ///   Gets or sets the vector of values indicating which non-zero
        ///   value happens at each position indicated in <see cref="Indices"/>.
        ///   Must set values first, if setting individually
        /// </summary>
        /// 
        public T[] Values
        {
            get { return values; }
            set
            {
                if (!Equals(value, default(T)))
                    values = value; UpdateMap();
            }
        }

        public int RowsLength => rowsLength;

        public int ColsLength => colsLength;

        public void UpdateMap()
        {
            Map = new Dictionary<Tuple<int,int>,T>();
            for (int i = 0; i < indices.Length; i++)
                if (!Equals(values[i], default(T)))
                    Map.Add(indices[i], values[i]);
        }
        
        public void UpdateIndicesAndValues()
        {
            indices = new Tuple<int, int>[Map.Keys.ToArray().Length];
            values = new T[Map.Keys.ToArray().Length];   
            for (int i = 0; i < Map.Keys.ToArray().Length; i++)
            {
                indices[i] = Map.Keys.ToArray()[i];
                values[i] = Map.Values.ToArray()[i];
            }
        }

        

        /// <summary>
        ///   Creates a sparse vector with the maximum number of elements.
        /// </summary>
        /// 
        /// <param name="length">The maximum number of non-zero
        ///   elements that this vector can accommodate.</param>
        ///   
        public SparseMatrix(int length, int n)
        {
            this.indices = new Tuple<int,int>[length];
            this.values = new T[length];
            this.Map = new Dictionary<Tuple<int,int>,T>();
        }

        private int GetNonZeroLength(T[] a)
        {
            int count = 0;
            
            for (int i = 0; i < a.Length; i++)
            {
                if (Equals(a[i], default(T)))
                {
                    continue;
                }
                else
                {
                    count++;
                }
            }
            
            return count;
        }

        /// <summary>
        ///   Creates a sparse Matrix from a vector of Tuples holding the indices
        ///   and a vector of values occurring at those indices.
        /// </summary>
        /// 
        /// <param name="indices">The indices for non-zero entries.</param>
        /// <param name="values">The non-zero values happening at each index.</param>
        /// 
        public SparseMatrix(Tuple<int,int>[] indices, T[] values)
        {
            int count = 0;

            this.nonZeroLength = GetNonZeroLength(values);
            this.rowsLength = indices.Select(i => i.Item1).Max() + 1;
            this.colsLength = indices.Select(i => i.Item2).Max() + 1;
            this.values = new T[this.nonZeroLength];
            this.indices = new Tuple<int, int>[this.nonZeroLength];
            this.Map = new Dictionary<Tuple<int, int>, T>();
            

            
            for (int i = 0; i < values.Length; i++)
            {
                if (object.Equals(values[i], default(T)))
                {
                    continue;
                }
                else
                {
                    this.values[count] = values[i];
                    this.indices[count] = indices[i];
                    this.Map.Add(this.indices[count], this.values[count]);
                    count++;
                }
            }
            
        }

        /// <summary>
        /// Counts the non-zero values in a matrix. Helper for the constructor that takes a matrix as input.
        /// </summary>
        /// <param name="a">Input matrix</param>
        /// <returns>Length of the matrix without zeros</returns>
        private int GetNonZeroLength(T[,] a)
        {
            int count = 0;
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (Equals(a[i, j],default(T)))
                    {
                        continue;
                    }
                    else
                    {
                        count++;
                    }
                }
            return count;
        }

        /// <summary>
        /// Counts the non-zero values in a matrix. Helper for the constructor that takes a matrix as input.
        /// </summary>
        /// <param name="a">Input matrix</param>
        /// <returns>Length of the matrix without zeros</returns>
        private int GetNonZeroLength(T[][] a)
        {
            int count = 0;
            for (int i = 0; i < a.Length; i++)
                for (int j = 0; j < a[i].Length; j++)
                {
                    if (Equals(a[i][j], default(T)))
                    {
                        continue;
                    }
                    else
                    {
                        count++;
                    }
                }
            return count;
        }

        /// <summary>
        /// Constructs a sparse matrix from a 'T' matrix. Finds the new length, indices(in tuples), and values within the constructor.
        /// </summary>
        /// <param name="a">Input Matrix</param>
        public SparseMatrix(T[,] a)
        {
            int count = 0;
            this.nonZeroLength = GetNonZeroLength(a);
            this.rowsLength = a.GetLength(0);
            this.colsLength = a.GetLength(1);
            this.values = new T[this.nonZeroLength];
            this.indices = new Tuple<int, int>[this.nonZeroLength];
            this.Map = new Dictionary<Tuple<int, int>, T>();
            
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (object.Equals(a[i,j], default(T)))
                    {
                        continue;
                    }
                    else
                    {
                        values[count] = a[i, j];
                        indices[count] = new Tuple<int, int>(i, j);
                        Map.Add(indices[count], values[count]);
                        count++;
                    }

                }
            }
        }

        /// <summary>
        /// Constructs a sparse matrix from a 'T' matrix. Finds the new length, indices(in tuples), and values within the constructor.
        /// </summary>
        /// <param name="a">Input Matrix</param>
        public SparseMatrix(T[][] a)
        {
            int count = 0;
            this.nonZeroLength = GetNonZeroLength(a);
            this.rowsLength = a.Length;
            this.colsLength = a[0].Length;
            this.values = new T[this.nonZeroLength];
            this.indices = new Tuple<int, int>[this.nonZeroLength];
            this.Map = new Dictionary<Tuple<int, int>, T>();

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    if (object.Equals(a[i][j], default(T)))
                    {
                        continue;
                    }
                    else
                    {
                        values[count] = a[i][j];
                        indices[count] = new Tuple<int, int>(i, j);
                        Map.Add(indices[count], values[count]);
                        count++;
                    }

                }
            }
        }

        /// <summary>
        ///   Converts this sparse matrix to a dense matrix of the given length.
        /// </summary>
        /// 
        public T[][] ToDense(int length)
        {
            T[][] result = new T[length][];
            for (int i = 0; i < Indices.Length; i++)
            {
                result[i] = new T[length];
                result[Indices[i].Item1][Indices[i].Item2] = Values[i];
            }
            return result;
        }

        /// <summary>
        ///   Converts this sparse matrix to a sparse representation where 
        ///   the indices are intertwined with their corresponding values.
        /// </summary>
        /// 
        public T[] ToSparse()
        {
            T[] result = new T[Indices.Length * 2];
            for (int i = 0; i < Indices.Length; i++)
            {
                result[2 * i + 0] = (T)System.Convert.ChangeType(Indices[i], typeof(T));
                result[2 * i + 1] = Values[i];
            }

            return result;
        }

        /// <summary>
        ///   Creates a new object that is a copy of the current instance.
        /// </summary>
        /// 
        /// <returns>
        ///   A new object that is a copy of this instance.
        /// </returns>
        /// 
        public object Clone() => new SparseMatrix<T>((Tuple<int, int>[])indices.Clone(), (T[])values.Clone());


        /// <summary>
        ///   Gets the number of non-zero elements in the sparse vector.
        /// </summary>
        /// 
        public int Length => Indices.Length;

        /// <summary>
        /// Way of indexing the matrix
        /// </summary>
        /// <param name="i">index one</param>
        /// <param name="j">index two</param>
        /// <returns>The 'T' elemnt at index 'i' and 'j'</returns>
        public T GetElementAt(int i, int j) => Map[new Tuple<int, int>(i, j)];

        public T GetElementAt(Tuple<int, int> t) => Map[t];

        public void SetElementAt(int i, int j , T value)
        {
            Map[new Tuple<int,int>(i,j)] = value;
            UpdateIndicesAndValues();
        }

        public void SetElementAt(Tuple<int,int> t, T value)
        {
            Map[t] = value;
            UpdateIndicesAndValues();
        }

        public int GetLength(int dimension) => 0;

        public T[] GetRow(int index)
        {

            Tuple<int,int>[] a = indices.Where(i => i.Item1.Equals(index)).ToArray();
            T[] result = new T[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                result[i] = Map[a[i]];
            }
            return result;
        }

        public int GetRowLength(int index) => indices.Where(i => i.Item1.Equals(index)).ToArray().Length;

        #region Interface Satisfiers

        int IList<T>.IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        void IList<T>.Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        void IList<T>.RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        T IList<T>.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        void ICollection<T>.Add(T item)
        {
            throw new NotImplementedException();
        }

        void ICollection<T>.Clear()
        {
            Array.Clear(values, 0, values.Length);
        }

        bool ICollection<T>.Contains(T item)
        {
            throw new NotImplementedException();
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        int ICollection<T>.Count => 0;

        bool ICollection<T>.IsReadOnly => false;

        bool ICollection<T>.Remove(T item)
        {
            throw new NotImplementedException();
        }

        

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        int IList.Add(object value)
        {
            throw new NotImplementedException();
        }

        void IList.Clear()
        {
            Array.Clear(values, 0, values.Length);
        }

        bool IList.Contains(object value)
        {
            throw new NotImplementedException();
        }

        int IList.IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        void IList.Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        bool IList.IsFixedSize => true;

        bool IList.IsReadOnly => false;

        void IList.Remove(object value)
        {
            throw new NotImplementedException();
        }

        void IList.RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        int ICollection.Count => 0;

        bool ICollection.IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        object ICollection.SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() => ToString("g", CultureInfo.CurrentCulture);

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            var sb = new StringBuilder();
            sb.Append("{");
            for (int i = 0; i < Indices.Length; i++)
            {
                sb.Append(Indices[i]);
                sb.Append(":");
                sb.AppendFormat(formatProvider, "{0:" + format + "}", Values[i]);
                if (i < Indices.Length - 1)
                    sb.Append(" ");
            }
            sb.Append("}");
            return sb.ToString();
        }
    }
}
