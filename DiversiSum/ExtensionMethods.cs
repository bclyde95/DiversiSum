﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using System.Numerics;

namespace DiversiSum
{
    public static class ExtensionMethods
    {
        #region SparseMatrix<T>

        /// <summary>
        /// Returns the matrix with all the zeros back
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mat">instance of SparseMatrix</param>
        /// <returns></returns>
        public static T[,] ToOriginalMatrix<T>( this SparseMatrix<T> mat)
        {
            
            int rows = mat.RowsLength;
            int cols = mat.ColsLength;
            
            T[,] output = new T[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    if (mat.Indices.Contains(new Tuple<int, int>(i, j)))
                    {
                        output[i, j] = mat[i,j];
                    }
                    else
                    {
                        output[i, j] = default(T);
                    }
                }
            }

            return output;
        }

        public static T[][] ToOriginalJagged<T>(this SparseMatrix<T> mat)
        {

            int rows = mat.RowsLength;
            int cols = mat.ColsLength;

            T[][] output = new T[rows][];
            for (int i = 0; i < rows; i++)
            {
                output[i] = new T[cols];
                for (int j = 0; j < rows; j++)
                {
                    if (mat.Indices.Contains(new Tuple<int, int>(i, j)))
                    {
                        output[i][j] = mat[i,j];
                    }
                    else
                    {
                        output[i][j] = default(T);
                    }
                }
            }

            return output;
        }



        /// <summary>
        /// Converts the SparseMatrix to a jagged matrix in it's current form. Useful for inputting into functions.
        /// </summary>
        /// <typeparam name="T">The current type of the SparseMatrix</typeparam>
        /// <param name="mat">the instance of SparseMatrix</param>
        /// <returns>A jagged array of the same type, with the same values but without the original indices</returns>
        public static T[][] ToZerolessJagged<T>(this SparseMatrix<T> mat)
        {
            T[][] result = new T[mat.RowsLength][];
            for (int i = 0; i < mat.RowsLength; i++)
            {
                result[i] = mat.GetRow(i);
            }
            return result;
        }


        public static double[][] ToMagnitude(this SparseMatrix<Complex> mat)
        {
            double[][] result = new double[mat.RowsLength][];
            for (int i = 0; i < mat.RowsLength; i++)
            {
                result[i] = new double[mat.ColsLength];
                for (int j = 0; j < mat.ColsLength; j++)
                {
                    result[i][j] = mat[i, j].Magnitude;
                }
            }
            return result;
        }
        

        public static bool IsSymmetric<T>(this SparseMatrix<T> mat)
        {
            if (mat == null) throw new ArgumentNullException("Matrix is null");

            if (mat.RowsLength == mat.ColsLength)
            {
                for (int i = 0; i < mat.RowsLength; i++)
                {
                    for (int j = 0; j <= i; j++)
                    {
                        if (!mat[i, j].Equals(mat[j, i]))
                            return false;
                    }
                }
                return true;
            }

            return false;
        }

        public static void Apply<T> (this SparseMatrix<T> mat, Func<T,T> F)
        {
            for (int i = 0; i < mat.RowsLength; i++)
            {
                for (int j = 0; j < mat.ColsLength; j++)
                {
                    mat[i, j] = F(mat[i, j]);
                }
            }
        }

#endregion

        #region Complex[][]

        // Summary:
        //     Computes the product A*B of matrix A and diagonal matrix B.
        //
        // Parameters:
        //   a:
        //     The left matrix A.
        //
        //   b:
        //     The diagonal vector of right matrix B.
        //
        // Returns:
        //     The product A*B of the given matrices A and B.
        public static Complex[][] DotWithDiagonal(this Complex[][] a, Complex[] b)
        {
            double[][] realMat = new double[a.Length][];
            double[][] imaginaryMat = new double[a.Length][];
            double[] realVec = new double[b.Length];
            double[] imaginaryVec = new double[b.Length];

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    realMat[i][j] = a[i][j].Real;
                    imaginaryMat[i][j] = a[i][j].Imaginary;
                }
            }

            for (int i = 0; i < b.Length; i++)
            {
                realVec[i] = b[i].Real;
                imaginaryVec[i] = b[i].Imaginary;
            }

            double[][] resultReal = Accord.Math.Matrix.DotWithDiagonal(realMat, realVec);
            double[][] resultImagi = Accord.Math.Matrix.DotWithDiagonal(imaginaryMat, imaginaryVec);
            Complex[][] result = new Complex[resultReal.Length][];

            for (int i = 0; i < resultReal.Length; i++)
            {
                for (int j = 0; j < resultReal[i].Length; j++)
                {
                    result[i][j] = new Complex(resultReal[i][j], resultImagi[i][j]);
                }
            }
            return result;
        }

        /// <summary>
        /// Divides two jagged matrices by multiplying the original by the inverse of the other
        /// </summary>
        /// <param name="a">instance of Complex[][]</param>
        /// <param name="b">Complex Matrix to divide by</param>
        /// <returns></returns>
        public static Complex[][] Divide(this Complex[][] a, Complex[][] b)
        {
            double[][] realMatA = new double[a.Length][];
            double[][] imaginaryMatA = new double[a.Length][];

            double[][] realMatB = new double[b.Length][];
            double[][] imaginaryMatB = new double[b.Length][];

            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    for (int j = 0; j < a[i].Length; j++)
                    {
                        realMatA[i][j] = a[i][j].Real;
                        imaginaryMatA[i][j] = a[i][j].Imaginary;

                        realMatB[i][j] = b[i][j].Real;
                        imaginaryMatB[i][j] = b[i][j].Imaginary;
                    }
                }
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    for (int j = 0; j < a[i].Length; j++)
                    {
                        realMatA[i][j] = a[i][j].Real;
                        imaginaryMatA[i][j] = a[i][j].Imaginary;
                    }
                }
                for (int i = 0; i < b.Length; i++)
                {
                    for (int j = 0; j < b[i].Length; j++)
                    {
                        realMatB[i][j] = b[i][j].Real;
                        imaginaryMatB[i][j] = b[i][j].Imaginary;
                    }
                }
            }

            double[][] resultReal = Accord.Math.Matrix.Divide(realMatA, realMatB);
            double[][] resultImagi = Accord.Math.Matrix.Divide(imaginaryMatA, imaginaryMatB);
            Complex[][] result = new Complex[resultReal.Length][];

            for (int i = 0; i < resultReal.Length; i++)
            {
                for (int j = 0; j < resultReal[i].Length; j++)
                {
                    result[i][j] = new Complex(resultReal[i][j], resultImagi[i][j]);
                }
            }

            return result;
        }

        public static Complex[][] Divide(this Complex[][] a, Complex b)
        {
            double[][] realMatA = new double[a.Length][];
            double[][] imaginaryMatA = new double[a.Length][];

            double realB = b.Real;
            double imaginaryB = b.Imaginary;

            
            for (int i = 0; i < a.GetLength(0); i++)
            {
                realMatA[i] = new double[a[i].Length];
                imaginaryMatA[i] = new double[a[i].Length];
                for (int j = 0; j < a[i].Length; j++)
                {
                    realMatA[i][j] = a[i][j].Real;
                    imaginaryMatA[i][j] = a[i][j].Imaginary;
             
                }
            }
            
            

            double[][] resultReal = realMatA.Divide(realB);
            double[][] resultImagi = imaginaryMatA.Divide(imaginaryB);
            Complex[][] result = new Complex[resultReal.Length][];

            for (int i = 0; i < resultReal.Length; i++)
            {
                result[i] = new Complex[a[i].Length];
                for (int j = 0; j < resultReal[i].Length; j++)
                {
                    double im =  resultImagi[i][j];
                    if (double.IsNaN(im))
                        im = 0;
                    result[i][j] = new Complex(resultReal[i][j], im);
                }
            }

            return result;
        }

        public static Complex Sum (this Complex[][] mat)
        {
            double realSum = 0;
            double imaginarySum = 0;

            for (int i = 0; i < mat.Length; i++)
            {
                for (int j = 0; j < mat[i].Length; j++)
                {
                    realSum += mat[i][j].Real;
                    imaginarySum += mat[i][j].Imaginary;
                }
            }

            return new Complex(realSum, imaginarySum);
        }

        public static double[][] RealToDouble(this Complex[][] mat)
        {
            double[][] result = new double[mat.Length][];
            for (int i = 0; i < mat.Length; i++)
            {
                result[i] = new double[mat[i].Length];
                for (int j = 0; j < mat[i].Length; j++)
                    result[i][j] = mat[i][j].Real;
            }
            return result;
        }

        public static double[][] ImaginaryToDouble(this Complex[][] mat)
        {
            double[][] result = new double[mat.Length][];
            for (int i = 0; i < mat.Length; i++)
            {
                result[i] = new double[mat[i].Length];
                for (int j = 0; j < mat[i].Length; j++)
                    result[i][j] = mat[i][j].Imaginary;
            }
            return result;
        }

        public static double[][] ToMagnitude(this Complex[][] mat)
        {
            double[][] result = new double[mat.Length][];
            for (int i = 0; i < mat.Length; i++)
            {
                result[i] = new double[mat[i].Length];
                for (int j = 0; j < mat[i].Length; j++)
                {
                    result[i][j] = mat[i][j].Magnitude;
                }
            }
            return result;
        }

        public static bool IsSymmetric(this Complex[][] a)
        {
            if (a == null) throw new ArgumentNullException("Matrix is null");

            if (a.Length == a[0].Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    for (int j = 0; j <= i; j++)
                    {
                        if (a[i][j] != a[j][i])
                            return false;
                    }
                }
                return true;
            }

            return false;
        }

        #endregion

        #region double[][]

        /// <summary>
        /// Finds the sum of each column and returns an array of the sums
        /// </summary>
        /// <param name="mat">The initial matrix</param>
        /// <returns>The column sums as an array</returns>
        public static double[] SumEachColumn(this double[][] mat)
        {
            double[] result = new double[mat.Length];

            if (mat.Length == mat[mat.Length - 1].Length)
            {
                for (int j = 0; j < mat.Length; j++)
                {
                    for (int i = 0; i < mat.Length; i++)
                    {
                        result[j] += mat[i][j];
                    }
                }
            }
            else
                throw new Exception("Matrix must be square");

            return result;
        }

        /// <summary>
        /// Creates a transition matrix from the input matrix, then either saves to the initial variable or outputs a new matrix
        /// </summary>
        /// <param name="mat">The initial matrix</param>
        /// <param name="inPlace">"true" if you want it to save to the same variable, "false" if you want to output a new matrix instead</param>
        /// <returns>A new matrix if inPlace == false</returns>
        public static double[][] ToTransition(this double[][] mat, bool inPlace)
        {
            if (inPlace)
            {
                double[] sums = mat.SumEachColumn();
                for (int i = 0; i < mat.Length; i++)
                {
                    for (int j = 0; j < mat[i].Length; j++)
                    {
                        mat[i][j] = mat[i][j] / sums[j];
                    }
                }
                return mat;
            }
            else
            {
                double[][] result = new double[mat.Length][];
                double[] sums = mat.SumEachColumn();
                for (int i = 0; i < mat.Length; i++)
                {
                    result[i] = new double[mat[i].Length];
                    for (int j = 0; j < mat[i].Length; j++)
                    {
                        result[i][j] = mat[i][j] / sums[j];
                    }
                }
                return result;
            }
        }

        public static double[][] ColumnMult(this double[][] mat, double[][] mul,int index)
        {
            double[][] result = new double[mat.Length][];
            for (int i = 0; i < mat.Length; i++)
            {
                result[i] = new double[1];
                if (mul[i][index] == 0)
                    result[i][0] = mat[i][index] * 1;
                else
                    result[i][0] = mat[i][index] * mul[i][index];

            }
            return result;
        }

        public static double[][] RowMult(this double[][] mat, double[][] rowVector, int index)
        {
            double[][] result = new double[1][] { new double [mat[0].Length]};
            for (int i = 0; i < mat[0].Length; i++)
            {
                if (rowVector[0][i] == 0)
                    result[0][i] = mat[index][i] * 1;
                else
                    result[0][i] = mat[index][i] * rowVector[0][i];
            }
            return result;
        }

        #endregion

        #region T[][]



        #endregion
    }
}
